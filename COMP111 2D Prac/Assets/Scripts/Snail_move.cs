﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snail_move : MonoBehaviour {
    Rigidbody2D rb;
    public float speed;
    public Transform Left;
    public Transform Right;
    Vector2 pos;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = (new Vector2(speed, 0.0f));
        pos = rb.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if((rb.transform.position.x < Left.position.x && rb.velocity.x < 0) || (rb.transform.position.x > Right.position.x && rb.velocity.x > 0))
        {
            Vector2 v = rb.velocity;
            v.x = -v.x;
            rb.velocity = v;
            flip();
        }
	}

    void flip()
    {
        Vector3 scale = transform.localScale;
        scale.x = -scale.x;
        rb.transform.localScale = scale;
    }
}
