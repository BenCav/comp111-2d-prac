﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Player_Controller2 : MonoBehaviour {

    public GameObject bullet;
    public Transform gun;

    public AudioClip coinSound;
    public AudioSource coinSource;
    public float water;
    bool swimming = false;

    int score = 0;
    int bonus = 0;
    public Text countScore;
    public Text winText;

    Animator ani;
    public float maxSpeed = 10f;
    Rigidbody2D rb;
    bool facingRight = true;
    bool Crouch = false;
    float crouchSpeed;
    public float crouchSpeedChange = 0.1f;
    bool inwater = false;

    bool grounded = false;
    public Transform groundCheck;
    float groundRadius = 0.2f;
    public LayerMask whatIsGround;
    public float jumpSpeed = 700;

	// Use this for initialization
	void Start () {
        coinSource.clip = coinSound;
        rb = GetComponent<Rigidbody2D>();
        ani = GetComponent<Animator>();
        countScore.text = "Score: " + score.ToString();
        winText.text = "";
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetKey(KeyCode.S)){
            ani.SetBool("Crouch", true);
            crouchSpeed = crouchSpeedChange;
        }
        else
        {
            ani.SetBool("Crouch", false);
            crouchSpeed = 1;
        }
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        ani.SetBool("Ground", grounded);

        float move = Input.GetAxis("Horizontal");
        ani.SetFloat("Speed", Mathf.Abs(move));

        Vector2 waterJump = new Vector2(0.0f, water);

        rb.velocity = new Vector2(move*maxSpeed*crouchSpeed, rb.velocity.y);

        if(move > 0 && !facingRight)
        {
            Flip();
        }else if(move < 0 && facingRight)
        {
            Flip();
        }
		
	}

    void Update()
    {
        if((grounded && Input.GetKeyDown(KeyCode.Space)) || (inwater && rb.velocity.y <0) || (grounded && Input.GetKeyDown(KeyCode.W)))
        {
            ani.SetBool("Ground", false);
            rb.AddForce(new Vector2(0, jumpSpeed));
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Instantiate(bullet, gun);
            bullet.transform.parent = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
            score += 10;
            coinSource.Play();
            Score();
            if (score == 140)
            {
                winText.text = "you win! you got all 14 coins";
            }
        }
        if (other.gameObject.CompareTag("Water"))
        {
            inwater = true;
        }
        if (other.gameObject.CompareTag("Meme"))
        {
            other.gameObject.SetActive(true);
        }
        if (other.gameObject.CompareTag("Key"))
        {
            bonus += 1000;
            other.gameObject.SetActive(false);
            coinSource.Play();
            Score();
        }
        if (other.gameObject.CompareTag("Item"))
        {
            bonus += 50;
            Score();
            coinSource.Play();
            float tempx = transform.localScale.x;
            float tempy = transform.localScale.y;
            Vector3 scale = new Vector3(tempx*2, tempy*2, 0.0f);
            transform.localScale = scale;
            jumpSpeed = jumpSpeed * 1.4f;
            other.gameObject.SetActive(false);

        }
    }

    void Score()
    {
        int temp = score + bonus;
        countScore.text = "Score: " + temp.ToString();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            inwater = false;
        }
    }



    void Flip()
    {
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x = scale.x * -1;
        transform.localScale = scale;
    }
}
