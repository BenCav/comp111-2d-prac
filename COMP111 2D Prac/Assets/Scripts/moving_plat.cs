﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moving_plat : MonoBehaviour {
    public float offset = 10;
    Vector2 origin;
    Rigidbody2D rb;
    public float speed = 2;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        origin = rb.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if(rb.transform.position.x <= origin.x)
        {
            rb.velocity = transform.right * speed;
        } else if (rb.transform.position.x > origin.x + offset)
        {
            rb.velocity = -transform.right * speed;
        }
	}
}
