﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation : MonoBehaviour {

    Rigidbody2D rb;
    public float speed = 0.1f;
    Vector2 origin;
    public float buffer = 3f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        origin = transform.position;
    }

    // Update is called once per frame
	void Update () {
        if (transform.position.y <= origin.y)
            rb.velocity = transform.up * speed;
        else if (transform.position.y >= origin.y + buffer)
            rb.velocity = -transform.up * speed;
    }
}
