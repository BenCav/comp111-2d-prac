﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour {

    Rigidbody2D rb;
    public float speed;
    public float jumpSpeed;
    public GameObject floor;
    public float offset;

	void Start () {
        rb = GetComponent<Rigidbody2D>();

	}
	
	void FixedUpdate () {
        float moveVertical;
        if(floor.transform.position.y + offset <= transform.position.y)
        {
            moveVertical = 0.0f;
        }
        else
        {
            moveVertical = Input.GetAxis("Vertical");
        }
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector2 move = new Vector2(moveHorizontal, moveVertical*jumpSpeed);
        rb.AddForce(move * speed);
        //transform.position = moveHorizontal;
	}
}
